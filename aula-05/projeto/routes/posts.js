// ROUTE posts.js
const express = require('express');
const router = express.Router();

const postsController = require('../controllers/posts');

router.get('/', postsController.get);
router.post('/novo', postsController.novo);

module.exports = router;