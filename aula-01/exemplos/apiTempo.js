const axios = require('axios');
const $ = require('string');

axios.get('https://api.hgbrasil.com/weather?woeid=BRXX0137')
     .then(
         (res) => {
             if($(res.data.results.description).contains('nublado')) {
                console.log('O tempo hoje está nublado!');
             } else {
                console.log('Passe protetor :)');
             }
             
         }
     )
     .catch(
         (error) => {
             console.log(error);
         }
     );
